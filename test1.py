class Solution(object):

    step = 1

    def __init__(self, n):
        self.n = n
        self.result = 0

    def fib(self):
        n = self.n
        def fn(n):
            if n == 0:
                return 0
            elif n == 2 or n == 1:
                return 1
            elif n > 2:
                self.result = fn(n-1) + fn(n-2)
                return self.result
        return fn(n)

res = Solution(9)
print(res.fib())