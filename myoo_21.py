# 测试工厂模式和单例模式的混合

class CarFactory:

    __obj = None
    __init_flag = True

    def __new__(cls, *args, **kwargs):
        if cls.__obj == None:
            cls.__obj = object.__new__(cls)

        return cls.__obj

    def __init__(self):
        if CarFactory.__init_flag:
            print("init......")
            CarFactory.__init_flag = False

    def create_car(self, brand):
        if brand == "Benz":
            return Benz()
        elif brand == "BMW":
            return BMW()
        elif brand == "BYD":
            return BYD()
        else:
            return "The brand is unknown!"


class Benz:
    pass


class BMW:
    pass


class BYD:
    pass


factory = CarFactory()
c1 = factory.create_car("Benz")
c2 = factory.create_car("BMW")
c3 = factory.create_car("BYD")
print(c1)
print(c2)

factory2 = CarFactory()
print(factory)
print(factory2)
