class Student:  # 类名一般首字母大写，多个单词采用驼峰原则

    def __init__(self, name, score):  # self必须要位于第一个参数
        self.name = name
        self.score = score

    def say_score(self):  # self必须要位于第一个参数
        print("{0}的分数是：{1}".format(self.name, self.score))


s1 = Student("Jiabing", 18)  # 通过类名()来调用构造函数
s1.say_score()
s1.age = 32
s1.salary = 2000
print(s1.salary)

s2 = Student("Gaoqi", 30)
print(dir(s1))
print(s1.__dict__)
print(isinstance(s1, Student))