import matplotlib.pyplot as plt
import numpy as np
from numpy.fft import fft


def add(a, b):
    """计算两个数字之和"""
    print("计算两个数字之和:{0}+{1}={2}".format(a, b, a+b))
    return a+b


add(1, 3)
help(add.__doc__)

size = 512
n = np.concatenate((np.arange(1, size / 2 + 1, 2, dtype=int),
                    np.arange(size / 2 - 1, 0, -2, dtype=int)))


f = np.zeros(size)
f[0] = 0.25
f[1::2] = -1 / (np.pi * n) ** 2
fourier_filter = 2 * np.real(fft(f))
#plt.plot(n)
plt.plot(fourier_filter)
plt.plot(fourier_filter)
plt.show()