# 测试多态

class Man:
    def eat(self):
        print("饿了，吃饭啦！！！")


class Chinese(Man):
    def eat(self):
        print("中国人用筷子吃饭")


class English(Man):
    def eat(self):
        print("英国人用叉子吃饭")


class Indian(Man):
    def eat(self):
        print("印度人用手吃饭")


def man_eat(m):
    if isinstance(m, Man):
        m.eat() # 不同子类的对象调用不同的eat方法，即多态
    else:
        print("不能吃饭")


man_eat(Chinese())
man_eat(English())