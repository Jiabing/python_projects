# 测试类方法、静态方法、析构方法

class Student:

    company = "SXT"

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def __del__(self):
        print("销毁对象：{0}".format(self))

    @classmethod
    def print_company(cls):
        print(cls.company)

    @staticmethod
    def add(a, b):
        print("{0}+{1}={2}".format(a, b, (a+b)))
        return a+b

class Person:

    def __del__(self):
        print("销毁完毕！！！")


# Student.print_company()
# Student.add(20, 30)
s1 = Student("s1", 20)
s2 = Student("s2", 30)
p1 = Person()
print(s1)
del s1