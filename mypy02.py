import turtle

t = turtle.Pen()

n_grids = 10
grid_size = 5
x = y = [x*n_grids*grid_size-n_grids*grid_size*2 for x in range(n_grids+1)]

for i in range(n_grids+1):
    t.penup()
    t.goto(x[0], y[i])
    t.pendown()
    t.goto(x[-1], y[i])
    t.penup()
    t.goto(x[i], y[0])
    t.pendown()
    t.goto(x[i], y[-1])

turtle.done()