import numpy as np
import matplotlib.pyplot as plt
import astra

# Create phantom.
print("Creating phantom……………………………………")
phantom=np.loadtxt("C:\\Users\\Jiabing\\Desktop\\CT仿真\\shepp_logan.txt")
print("Phantom completed!!!")

# Create geometries and projector.
print("Projection processing……………………………………")
vol_geom = astra.create_vol_geom(512, 512)
angles = np.linspace(0, np.pi, 180*4, endpoint=False)
# proj_geom = astra.create_proj_geom('parallel', 1., 512, angles)
# projector_id = astra.create_projector('linear', proj_geom, vol_geom)
proj_geom = astra.create_proj_geom('fanflat', 0.5, 2000, angles, 700, 700)
projector_id = astra.create_projector('line_fanflat', proj_geom, vol_geom)

# Create sinogram.
sinogram_id, sinogram = astra.create_sino(phantom, projector_id)
print("Sinogram completed!!!")
plt.pcolor(sinogram, cmap='gray')
plt.show()

print("Reconstructing………………………………")
# Create reconstruction.
reconstruction_id = astra.data2d.create('-vol', vol_geom)
cfg = astra.astra_dict('SIRT')
cfg['ReconstructionDataId'] = reconstruction_id
cfg['ProjectionDataId'] = sinogram_id
cfg['ProjectorId'] = projector_id
cfg['option'] = {}
cfg['option']['MinConstraint'] = 0.  # Force solution to be nonnegative.
algorithm_id = astra.algorithm.create(cfg)
astra.algorithm.run(algorithm_id, 100)  # 100 iterations.
reconstruction = astra.data2d.get(reconstruction_id)
print("Reconstruction completed!!!")

plt.pcolor(reconstruction, cmap="gray")
plt.show()

# Cleanup.
astra.algorithm.delete(algorithm_id)
astra.data2d.delete(reconstruction_id)
astra.data2d.delete(sinogram_id)
astra.projector.delete(projector_id)