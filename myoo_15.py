# 运算符的重载

class Person:
    def __init__(self, name):
        self.name = name

    def __add__(self, other):
        if isinstance(other, Person):
            return "{0}--{1}".format(self.name, other.name)
        else:
            return "不是同类对象，不能相加！！！"

    def __mul__(self, other):
        if isinstance(other, int):
            return self.name * other
        elif isinstance(other, Person):
            return "{0}*{1}".format(self.name, other.name)
        else:
            return "不是同类对象，不能相乘！！！"

p1 = Person("Jiabing")
p2 = Person("Jerry")

x = p1 + p2
print(x)
print(x * 3)    # 此处是用的字符串的乘法方法
print(p1 * 3)   # 此处是用户自己重载定义的乘法方法
print(p1 * p2)