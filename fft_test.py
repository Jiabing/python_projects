import matplotlib.pyplot as plt
import numpy.fft as fft
import numpy as np


x = np.linspace(0, 1, 100)
y = np.sin(100*x) + np.sin(200*x) + 100

y_fft = fft.fft(y, 128)
plt.plot(fft.ifft(y_fft), color="red")
plt.plot(y, color="blue")
plt.show()