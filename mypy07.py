# 测试嵌套函数（内部函数）的定义

def outer():
    print("outer running...")

    def inner01():
        print("inner01 running...")

    inner01()


def print_chinese_name(name, family_name):
    print("{0} {1}".format(family_name, name))


def print_english_name(name, family_name):
    print("{0} {1}".format(name, family_name))


def print_name(is_chinses, name, family_name):
    def inner_print(a, b):
        print("{0} {1}".format(a, b))

    if is_chinses:
        inner_print(family_name, name)
    else:
        inner_print(name, family_name)


print_name(False, "Jerry", "Xiang")
print_name(True, "Jiabing", "Xiang")