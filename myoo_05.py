# 测试方法的动态性

class Person:

    def work(self):
        print("Work hard!!!")


def play_game(s):
    print("{0}在玩游戏！".format(s))


def work2():
    print("Good Good Study! Day Day UP!")


Person.play = play_game
p = Person()
p.work()
p.play()  # Person.play(p)

Person.work = work2
p.work()
