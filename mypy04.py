# 函数也是对象

# def test01():
#     print(a)
#     print("sxtsxt")
#
#
# test01()
# c = test01
# c()
# print(id(test01))
# print(id(c))
# print(type(c))


# 测试全局变量
# a = 300
# test01()

# 测试全局变量与局部变量的效率
import math
import time


def test01():
    start = time.time()
    for i in range(100000000):
        math.sqrt(30)
    end = time.time()
    print("test01耗时{0}".format(end-start))


def test02():
    b = math.sqrt
    start = time.time()
    for i in range(100000000):
        b(30)
    end = time.time()
    print("test02耗时{0}".format(end-start))


test01()
test02()