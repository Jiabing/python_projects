# 测试浅拷贝、深拷贝

import copy


def test01():
    a = [10, 20, [5, 6]]
    b = copy.copy(a)
    print("a:", a)
    print("b:", b)

    b.append(30)
    b[2].append(7)
    print("#####浅拷贝######")
    print("a:", a)
    print("b:", b)


def test02():
    a = [10, 20, [5, 6]]
    b = copy.deepcopy(a)
    print("a:", a)
    print("b:", b)

    b.append(30)
    b[2].append(7)
    print("#####深拷贝######")
    print("a:", a)
    print("b:", b)


test01()
print("########################################")
test02()
# a = (1, 2, [1, 2])
# a[2][0] = 100
# print(a)