# 测试nonlocal，global关键字的用法

# a = 100
#
#
# def outer():
#     b = 10
#
#     def inner():
#         nonlocal b  # 声明外部函数的局部变量
#
#         print("inner b:", b)
#         b = 20
#
#         global a
#         a = 100
#
#     inner()
#     print("outer b:", b)
#
#
# outer()
# print("a:", a)


# 测试LEGB规则

# str = "global str"


def outer():

    # str = "outer"

    def inner():
        # str = "inner"
        print(str)

    inner()


outer()
