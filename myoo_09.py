# @修饰器的作用

class Circle:
    __pi = 3.14

    def __init__(self, r):
        self.r = r

    @property
    def pi(self):
        return self.__pi

    @pi.setter  # 用setter方法给属性复制
    def pi(self, pi):
        Circle.__pi = pi


circle1 = Circle(2)
print(circle1.pi)
circle1.pi = 3.1414926  # 通过修饰器来给属性赋值
# print(circle1._Circle__pi)
