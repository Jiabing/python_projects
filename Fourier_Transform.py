import matplotlib.pyplot as plt
import numpy as np


def fun(x):
    return x**2


x = np.linspace(0,1,100)
y = fun(x)

plt.plot(x, y, color='red')
plt.show()
