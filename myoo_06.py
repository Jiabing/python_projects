# 测试私有属性

class Employee:

    __company = "Beihang"

    def __init__(self, name, age):
        self.name = name
        self.__age = age  # 私有属性

    def __work(self):
        print("Work Hard!!!")  # 私有方法
        print("年龄：{0}".format(self.__age))  #在类内部可以直接调用私有属性或私有方法
        print("His company is {0}".format(Employee.__company))


e = Employee("Jiabing", 18)

print(e.name)
print(e._Employee__age)
# print(e.__age)  # 外部不能直接访问类的私有属性
# e.__work()  # 外部不能直接访问类的私有方法
# print(Employee.__company)
print(dir(e))
e._Employee__work()
