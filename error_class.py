# 测试定义异常类

class GenderException(BaseException):
    def __init__(self):
        super().__init__()
        self.errMsg = "性别只能设置成男或女！！！"


class Student():
    def __init__(self, name, gender):
        self.name = name
        self.setGender(gender)

    # 设置性别
    def setGender(self, gender):
        if gender == "男" or gender == "女":
            self.__gender = gender
        else:
            # 抛出异常（性别异常）
            raise GenderException()

    def getGender(self):
        return self.__gender
    def showInfo(self):
        print("我叫：%s  性别：%s" %(self.name, self.__gender))

try:
    stu = Student("学生1", "12345")
except BaseException as e:
    print(type(e))
    print(e.errMsg)