# 测试异常解决方案

def sub1(a, b):
    if a.isdigit() and b.isdigit():
        a = int(a)
        b = int(b)
        if b != 0:
            c = a / b
            print("商为：%g" %c )
        else:
            print("除数不能为0， 操作有误！")
    else:
        print("数字类型有误！")


def sub2(a, b):
    try:
        a = int(a)
        b = int(b)
        c = a / b
        print("商为：%g" % c)
    except:
        print("输入类型有误/除数不能为0")


def sub3(a, b):
    try:
        a = int(a)
        b = int(b)
        c = a / b
        print("商为：%g" % c)
    except ValueError:
        print("输入类型有误")
    except ZeroDivisionError:
        print("除数不能为0")
    except Exception:   # 父类异常
        print("其他异常")


def sub3(a, b):
    try:
        a = int(a)
        b = int(b)
        c = a / b
        print("商为：%g" % c)
    except (Exception, ValueError, ZeroDivisionError) as e:
        print(type(e))
        print(e.args)
        print("遇到异常！！！")

# a = input("请输入被除数:")
# b = input("请输入除数：")
# sub1(a, b)
# sub3(a, b)
try:
    file = open('123.txt', 'w', encoding='utf-8')
    file.write('Hello')
    file.write('World')
    file.write([1, 2, 3])
    print('写入完毕')
except Exception as e:
    print(e.args)
else:
    print("没有异常，操作成功")
finally:
    file.close()
    print('关闭文件，谢谢使用')