# 测试可调用方法__call__()

class SalaryAccount:
    """工资计算类"""

    def __call__(self, salary):
        print("算工资啦...")
        year_salary = salary * 12
        day_salary = salary // 22.5
        hour_salary = day_salary // 8

        return dict(year_salary=year_salary, month_salary=salary,
                    day_salary=day_salary, hour_salary=hour_salary)


s = SalaryAccount()
print(s(30000))