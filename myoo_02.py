class Student:  # 类名一般首字母大写，多个单词采用驼峰原则

    company = "SXT"  # 类属性
    count = 0  # 类属性

    def __init__(self, name, score):  # self必须要位于第一个参数
        self.name = name  # 实例属性
        self.score = score
        Student.count = Student.count + 1

    def say_score(self):  # 实例方法
        print(self.name, "的公司是：", Student.company)
        print(self.name, "的分数是：", self.score)


stu2 = Student

s1 = Student("Jiabing", 18)
s2 = stu2("Gaoqi", 30)

s1.say_score()
s2.say_score()

print("一共创建了{0}个Student对象".format(Student.count))
