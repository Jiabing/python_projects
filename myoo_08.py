# 类方法与静态方法

class Date:
    """重构构造__init__()方法应用"""
    day = 0
    month = 0
    year = 0

    def __init__(self, year=0, month=0, day=0):
        self.day = day
        self.month = month
        self.year = year

    @classmethod
    def from_string(cls, date_as_string):
        year, month, day = date_as_string.split("-")
        date = cls(year, month, day)
        return date

    @staticmethod
    def is_date_valid(date_as_string):
        """
        用来校验日期格式是否正确
        """
        year, month, day = date_as_string.split("-")
        return int(year) <= 3999 and int(month) <=12 and int(day) <= 31

date1 = Date.from_string("2022-11-16")
print(date1.year, date1.month, date1.day)
is_date = Date.is_date_valid("2022-11-105")
print(is_date)