#coding=utf-8


# b = 0.00
# if b:
#     print( "空的列表是flase")
#
# c = "Flase"
# if c:
#     print(c)
#
# d = 10
# if d:
#     print(d)
#
# if 3<d<100:
#     print("3<d<10")
#
# s = input("请输入一个数字：")
# print("s是小于10的数字" if int(s)<10 else "s是大于10的数字")

# score = int(input("请输入一个在0-100之间的数字："))
# grade = "ABCDE"
# if score > 100 or score<0:
#     score = int(input("输入错误！！！请重新输入一个在0-100之间的数字："))
# else:
#     num = score//10
#     if num < 6:
#         num = 5
#     if num == 10:
#         num = 9
#     print("分数为{0},等级为{1}".format(score,grade[9 - num]))

# num = 0
# while num <= 10:
#     print(num,end="\t")
#     num += 1
#
# num2 = 0
# sum_all = 0
# while num2 <= 100:
#     sum_all += num2
#     num2 += 1
# print(sum_all,sum(range(101)))

# for i in range(1,10):
#     for j in range(1,10):
#         if j <= i:
#             print("{0}*{1}={2}".format(i,j,(i*j)),end="\t")
#     print()

# for i in range(1,10):
#     for j in range(1,i+1):
#         print("{0}*{1}={2}".format(i,j,(i*j)),end="\t")
#     print()

# while True:
#     a = input("请输入一个字符（输入Q或q的时退出）：")
#     if a=="q" or a=="Q":
#         print("循环结束，退出")
#         break
#     else:
#         print(a)

# empNum = 0
# salarySum = 0
# salarys = []
# while True:
#     s = input("请输入员工的薪资（按Q或q结束）：")
#
#     if s.upper() == 'Q':
#         print("录入完成，退出")
#         break
#     if float(s)<0:
#         continue
#     empNum += 1
#     salarys.append(float(s))
#     salarySum += float(s)
# print("录入员工人数为{0},总共薪资为{1}".format(empNum,salarySum))

# names = ("x", "j", "b", "a")
# ages = (18, 16, 20, 25)
# jobs = ("teacher", "student", "worker")
# for name, age, job in zip(names,ages, jobs):
#     print("{0}--{1}--{2}".format(name,age,job))

# y = [x**2 for x in range(1,100) if x % 3 == 0]
# cells = [(row, col) for row in range(1,10) for col in range(1,10)]
# print(cells)

# my_text = "i love u and i also love my family my school my lovers"
# char_count = {c: my_text.count(c) for c in my_text}
# print(char_count)

